/*
 * Copyright (c) 2016, Roman Meyta <theshrodingerscat@gmail.com>
 * Copyright (c) 2020-2021 https://gitee.com/fsfzp888
 * All rights reserved
 */

#include <QApplication>
#include <QTranslator>
#include <iostream>

#include "Logger.h"
#include "WebcamWindow.h"

#pragma comment(lib, "strmiids.lib")
#pragma comment(lib, "strmbase.lib")

int main(int argc, char *argv[])
{
    logger_initFileLogger("run.log", 1024 * 1024, 5);
    logger_setLevel(LogLevel_DEBUG);
    LOG_INFO("Camera application start.");
    std::locale loc = std::locale::global(std::locale(""));
    QApplication app(argc, argv);
    QString pluginPath = app.applicationDirPath() + "/plugins";
    app.addLibraryPath(pluginPath);
    app.setWindowIcon(QIcon(":app_icon.jpg"));
    QTranslator translator;
    translator.load("translation/zh.qm");
    app.installTranslator(&translator);
    WebcamWindow window;
    window.show();
    int res = app.exec();
    std::locale::global(loc);
    LOG_INFO("Camera application exit.");
    return res;
}
