<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="../ImageFormats.cpp" line="187"/>
        <source>current image format not supported</source>
        <translation>当前摄像头捕获的图像格式不支持</translation>
    </message>
</context>
<context>
    <name>StillImageWorker</name>
    <message>
        <location filename="../WebcamWindow.cpp" line="656"/>
        <source>finish record video</source>
        <translation>视频录制完毕</translation>
    </message>
    <message>
        <source>start record video</source>
        <translation type="vanished">开始录制视频</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="667"/>
        <source>save three photos</source>
        <translation>正在保存三张照片，请等待</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="680"/>
        <source>unknown</source>
        <translation>无名</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="707"/>
        <source> has already beed saved.</source>
        <translation> 已保存.</translation>
    </message>
</context>
<context>
    <name>WebcamWindow</name>
    <message>
        <location filename="../WebcamWindow.cpp" line="48"/>
        <source>Turn On</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="49"/>
        <source>Turn Off</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="50"/>
        <source>Capture</source>
        <translation>拍一张</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="51"/>
        <source>Capture Three</source>
        <translation>拍三张</translation>
    </message>
    <message>
        <source>Record Video</source>
        <translation type="vanished">录屏</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="52"/>
        <source>Start Record Video</source>
        <translation>开始录制视频</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="53"/>
        <source>Stop Record Video</source>
        <translation>结束录制视频</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="54"/>
        <source>Devices</source>
        <translation>设备列表</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="56"/>
        <source>Resolutions</source>
        <translation>分辨率</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="58"/>
        <source>Output Path</source>
        <translation>输出路径</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="60"/>
        <source>Browser</source>
        <translation>浏览</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="61"/>
        <source>name</source>
        <translation>姓名</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="63"/>
        <source>Hardware trigger video time range(ms)</source>
        <translation>双击硬件按钮触发等待时间间隔(毫秒)</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="80"/>
        <source>Webcam</source>
        <translation>德辉医疗口腔监测</translation>
    </message>
    <message>
        <source>save image</source>
        <translation type="vanished">图片已保存</translation>
    </message>
    <message>
        <source>start capture video</source>
        <translation type="vanished">开始录制视频</translation>
    </message>
    <message>
        <source>stop capture video</source>
        <translation type="vanished">停止录制视频</translation>
    </message>
    <message>
        <source>save video</source>
        <translation type="vanished">开始录制视频</translation>
    </message>
    <message>
        <source>stop video</source>
        <translation type="vanished">停止录制视频</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="297"/>
        <location filename="../WebcamWindow.cpp" line="549"/>
        <source>unknown</source>
        <translation>无名</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="324"/>
        <source> has already beed saved.</source>
        <translation> 已保存</translation>
    </message>
    <message>
        <source>photo already saved</source>
        <translation type="vanished">照片已保存</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="469"/>
        <source>finish record video</source>
        <translation>视频录制完毕</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="539"/>
        <source>start record video</source>
        <translation>开始录制视频</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="593"/>
        <location filename="../WebcamWindow.cpp" line="595"/>
        <source> start recording.</source>
        <translation> 开始录制视频</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="468"/>
        <location filename="../WebcamWindow.cpp" line="602"/>
        <source>Video finish record.</source>
        <translation>视频录制完毕</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="412"/>
        <source>save one photos</source>
        <translation>正在保存一张照片</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="418"/>
        <source>save three photos</source>
        <translation>正在保存三张照片</translation>
    </message>
    <message>
        <location filename="../WebcamWindow.cpp" line="609"/>
        <source>Get output directory</source>
        <translation>获取输出路径</translation>
    </message>
</context>
</TS>
