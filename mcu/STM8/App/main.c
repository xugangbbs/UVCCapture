
#include "stm8s.h"

#define LED_GPIO_PORT  (GPIOC)
// 3: on board led, 5: green led, 7: red led
#define ON_BOARD_LED_PIN (GPIO_PIN_3)
#define GREEN_LED_PIN (GPIO_PIN_5)
#define RED_LED_PIN (GPIO_PIN_7)
#define LED_GPIO_PINS  (GPIO_PIN_3 | GPIO_PIN_5 | GPIO_PIN_7)

#define BUTTON_GPIO_PORT (GPIOA)
#define CAPTURE_PHOTO_PIN (GPIO_PIN_2)
#define CAPTURE_VIDEO_PIN (GPIO_PIN_3)
#define BUTTON_GPIO_PINS (GPIO_PIN_2 | GPIO_PIN_3)

#define CAMERA_GPIO_PORT (GPIOB)
#define CAMERA_GPIO_PINS (GPIO_PIN_5)

#define TIM4_PERIOD       124
__IO uint32_t TimingDelay = 0;
void Delay(__IO uint32_t nTime);
void TimingDelay_Decrement(void);
static void CLK_Config(void);
static void TIM4_Config(void);

void GPIO_Config()
{
  // led: on board led, green red led
  GPIO_Init(LED_GPIO_PORT, (GPIO_Pin_TypeDef)LED_GPIO_PINS, GPIO_MODE_OUT_PP_LOW_SLOW);
  
  // 1300W camera control signal
  GPIO_Init(CAMERA_GPIO_PORT, (GPIO_Pin_TypeDef)CAMERA_GPIO_PINS, GPIO_MODE_OUT_OD_HIZ_FAST);
  
  // capture photo
  GPIO_Init(BUTTON_GPIO_PORT, (GPIO_Pin_TypeDef)BUTTON_GPIO_PINS, GPIO_MODE_IN_PU_NO_IT);
  
  GPIO_WriteLow(LED_GPIO_PORT, (GPIO_Pin_TypeDef)RED_LED_PIN);
  GPIO_WriteHigh(LED_GPIO_PORT, (GPIO_Pin_TypeDef)GREEN_LED_PIN);
}

void main()
{
  /* Clock configuration -----------------------------------------*/
  CLK_Config();  

  /* GPIO configuration -----------------------------------------*/
  GPIO_Config();  

  /* TIM4 configuration -----------------------------------------*/
  TIM4_Config();   

  BitStatus photo_btn_sts;
  BitStatus video_btn_sts;
  int is_video_capture = 0;

  while (1)
  {
    /* Toggles LEDs */
    GPIO_WriteReverse(LED_GPIO_PORT, (GPIO_Pin_TypeDef)ON_BOARD_LED_PIN);
    GPIO_WriteHigh(CAMERA_GPIO_PORT, (GPIO_Pin_TypeDef)CAMERA_GPIO_PINS);
    Delay(100);
    photo_btn_sts = GPIO_ReadInputPin(BUTTON_GPIO_PORT, (GPIO_Pin_TypeDef)CAPTURE_PHOTO_PIN);
    if (photo_btn_sts == 0)
    {
      Delay(10);
      photo_btn_sts = GPIO_ReadInputPin(BUTTON_GPIO_PORT, (GPIO_Pin_TypeDef)CAPTURE_PHOTO_PIN);
      if (photo_btn_sts == 0)
      {
        while(GPIO_ReadInputPin(BUTTON_GPIO_PORT, (GPIO_Pin_TypeDef)CAPTURE_PHOTO_PIN) == 0);
        
        GPIO_WriteLow(CAMERA_GPIO_PORT, (GPIO_Pin_TypeDef)CAMERA_GPIO_PINS);
        Delay(300);
        GPIO_WriteHigh(CAMERA_GPIO_PORT, (GPIO_Pin_TypeDef)CAMERA_GPIO_PINS);
      }
    }
    video_btn_sts = GPIO_ReadInputPin(BUTTON_GPIO_PORT, (GPIO_Pin_TypeDef)CAPTURE_VIDEO_PIN);
    if (video_btn_sts == 0)
    {
      Delay(10);
      video_btn_sts = GPIO_ReadInputPin(BUTTON_GPIO_PORT, (GPIO_Pin_TypeDef)CAPTURE_VIDEO_PIN);
      if (video_btn_sts == 0)
      {
        while (GPIO_ReadInputPin(BUTTON_GPIO_PORT, (GPIO_Pin_TypeDef)CAPTURE_VIDEO_PIN) == 0);
        if (is_video_capture)
        {
          GPIO_WriteLow(LED_GPIO_PORT, (GPIO_Pin_TypeDef)RED_LED_PIN);
          GPIO_WriteHigh(LED_GPIO_PORT, (GPIO_Pin_TypeDef)GREEN_LED_PIN);
          is_video_capture = 0;
        }
        else
        {
          GPIO_WriteLow(LED_GPIO_PORT, (GPIO_Pin_TypeDef)GREEN_LED_PIN);
          GPIO_WriteHigh(LED_GPIO_PORT, (GPIO_Pin_TypeDef)RED_LED_PIN);
          is_video_capture = 1;
        }
        GPIO_WriteLow(CAMERA_GPIO_PORT, (GPIO_Pin_TypeDef)CAMERA_GPIO_PINS);
        Delay(300);
        GPIO_WriteHigh(CAMERA_GPIO_PORT, (GPIO_Pin_TypeDef)CAMERA_GPIO_PINS);
        Delay(900);
        GPIO_WriteLow(CAMERA_GPIO_PORT, (GPIO_Pin_TypeDef)CAMERA_GPIO_PINS);
        Delay(300);
        GPIO_WriteHigh(CAMERA_GPIO_PORT, (GPIO_Pin_TypeDef)CAMERA_GPIO_PINS);
      }
    }
  }

}

/**
  * @brief  Configure system clock to run at 16Mhz
  * @param  None
  * @retval None
  */
static void CLK_Config(void)
{
    /* Initialization of the clock */
    /* Clock divider to HSI/1 */
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
}

/**
  * @brief  Configure TIM4 to generate an update interrupt each 1ms 
  * @param  None
  * @retval None
  */
static void TIM4_Config(void)
{
  /* TIM4 configuration:
   - TIM4CLK is set to 16 MHz, the TIM4 Prescaler is equal to 128 so the TIM1 counter
   clock used is 16 MHz / 128 = 125 000 Hz
  - With 125 000 Hz we can generate time base:
      max time base is 2.048 ms if TIM4_PERIOD = 255 --> (255 + 1) / 125000 = 2.048 ms
      min time base is 0.016 ms if TIM4_PERIOD = 1   --> (  1 + 1) / 125000 = 0.016 ms
  - In this example we need to generate a time base equal to 1 ms
   so TIM4_PERIOD = (0.001 * 125000 - 1) = 124 */

  /* Time base configuration */
  TIM4_TimeBaseInit(TIM4_PRESCALER_128, TIM4_PERIOD);
  /* Clear TIM4 update flag */
  TIM4_ClearFlag(TIM4_FLAG_UPDATE);
  /* Enable update interrupt */
  TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);
  
  /* enable interrupts */
  enableInterrupts();

  /* Enable TIM4 */
  TIM4_Cmd(ENABLE);
}

/**
  * @brief  Inserts a delay time.
  * @param  nTime: specifies the delay time length, in milliseconds.
  * @retval None
  */
void Delay(__IO uint32_t nTime)
{
  TimingDelay = nTime;

  while (TimingDelay != 0);
}

/**
  * @brief  Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */
void TimingDelay_Decrement(void)
{
  if (TimingDelay != 0x00)
  {
    TimingDelay--;
  }
}

#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif
