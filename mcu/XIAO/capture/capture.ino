// set pin numbers:
const int buttonPin = 2;     // the number of the push button pin
const int outputPin = 3;
const int greenPin = 6;
const int redPin = 5;
const int ledPin =  13;      // the number of the LED pin

// variables will change:
//int buttonState = 0;         // variable for reading the push button status
int cnt = 0;

void setup(){
  pinMode(buttonPin, INPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(redPin, OUTPUT);
  pinMode(outputPin, OUTPUT);
  digitalWrite(ledPin, LOW);    // turn the LED off by making the voltage LOW
  //digitalWrite(greenPin, LOW);
  //digitalWrite(redPin, HIGH);
  digitalWrite(outputPin, LOW);
}

void loop(){
  // read the state of the pushbutton value:
  if (digitalRead(buttonPin)) {                                                                                                                                                               
    delay(5);
    if (digitalRead(buttonPin)) {
      while (digitalRead(buttonPin));
      digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
      digitalWrite(outputPin,HIGH);
      delay(330);
      digitalWrite(outputPin,LOW);
      delay(1200);
      digitalWrite(outputPin,HIGH);
      delay(300);
      digitalWrite(outputPin,LOW);
      digitalWrite(ledPin, LOW); 
      ++cnt; 
    }
  }
  if (cnt % 2)
  {
    digitalWrite(greenPin, LOW);
    digitalWrite(redPin, HIGH);
    digitalWrite(ledPin, HIGH); 
  }
  else
  {
    digitalWrite(greenPin, HIGH);
    digitalWrite(redPin, LOW);
    digitalWrite(ledPin, LOW);  
  }
  
}
